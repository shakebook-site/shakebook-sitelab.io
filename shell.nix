{ sources ? import ./nix/sources.nix {}
, haskellNix ? import sources.haskell {}
, pkgs ? import haskellNix.sources.nixpkgs-2003 haskellNix.nixpkgsArgs }:

let
  hsPkgs = import ./nix/default.nix {};
in
  hsPkgs.shellFor {
    withHoogle = true;

    packages = ps: with ps; [shakebook-site];
    buildInputs = with pkgs.haskellPackages;
    [ pkgs.fsatrace
      ghcid
      hlint
      stylish-haskell
      wai-app-static
      pkgs.ffmpeg
      pkgs.imagemagick
      pkgs.R
      pkgs.pkg-config
      pkgs.shake
      pkgs.texlive.combined.scheme-full
      pkgs.zlib.out
    ];

    LD_LIBRARY_PATH="${pkgs.zlib.out}/lib";
    exactDeps = true;
  }
