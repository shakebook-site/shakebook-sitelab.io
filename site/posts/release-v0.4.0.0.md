---
title: Shakebook v0.4.0.0 Released!
author: Daniel Firth
posted: 2020-05-26
tags: [shakebook, release]
description: Shakebook v0.4.0.0
---

This is a small release and will probably start a series of new smaller minor releases
as most of the functionality has been offloaded to shake-plus. This mainly just cleans up
the hardcoded config type that was present when defaults still lived in the library.

<!--more-->

* Add `withContent` lens.
* Add lifted version of `flattenMeta` from `Slick.Pandoc`
* Removed default `buildPDF` and moved it back to main repository.
* Remove `SBConfig` and constraints from this library. This was only here for refactoring
  convenience and creating a context should be up to the user.
