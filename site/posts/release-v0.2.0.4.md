---
title: Shakebook v0.2.0.4 Released!
author: Daniel Firth
posted: 2020-05-01
tags: [shakebook, release]
description: Shakebook v0.2.0.4
---

This release moves Shakebook from a templating repository to a full library with
an API, and a sensible set of defaults, tests and other general quality of life
improvements. You still sort of need to know what you're doing in order to
use it, but if you know Shake you shouldn't have many issues getting simple
sites to work.

<!--more-->

## Changelog
* Factored out API into library, available on [hackage](https://hackage.haskell.org/package/shakebook)
* Standardised lens and enrichment functions.
* Supports user-specified enrichments.
* Adds a Shakebook monad and a ShakebookA monad that wraps shake's
  Rules and shake's Action monads respectively.
* Supports reader based config of input and output directories, baseUrl,
  markdown reader and writer config options and posts per page.
* Supports more general pager specifications allowing user specified data
  extraction from the URL fragment into a page Zipper.
* Adds general loading function via `loadSortFilterExtract` for loading
  markdown via the monad through patterns.
* Add logging to Shakebook's monads via RIO's logging methods.
* Add testing framework.
* Add hackage documentation.
