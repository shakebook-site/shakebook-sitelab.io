---
title: Shakebook v0.3.0.1 Released!
author: Daniel Firth
posted: 2020-05-16
tags: [shakebook, release]
description: Shakebook v0.3.0.1
---

This release now places Shakebook firmly on top of shake-plus and the
documentation will be rewritten to introduce the approach as a shake-plus
ruleset. The shakebook-lib library itself has evaporated somewhat, mostly
leaving only things directly related to pandoc, mustache or default strings.

<!--more-->

The approach of keeping the default rules in `Shakebook.Defaults` has been
scrapped, and that is now back again in user control. This was a nightmare
originally, but now that shake-plus is a factor keeping the build logic in
feels considerably more compositional, and allows us to add caches which has
sped the built up by about 50x.

* Upgrade to [shake-plus](https://hackage.haskell.org/package/shake-plus) v0.1.3.0
  to take advantage of new interface consistency.
* readMarkdownFile now extracts images from the pandoc and calls need on them.
* Removed most default code, moved back to user level.
* Tempate now uses caching for loading posts resulting in significant speedup.
* Add withSocialLinks function.
* Remove "affix-style" json bolt-ons in favour of compositional approach.
* Add `sbGlobalApply` to apply a function on every page that comes into
  existence whether generated or loaded.
* Removed `Shakebook.Aeson` and moved to new library [aeson-with](https://hackage.haskell.org/package/aeson-with)
