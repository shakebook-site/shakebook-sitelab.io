---
title: Getting Started
description: Getting Started
modified: 2020-08-06T10:31:00Z
---

# Getting Started

You will need the [nix](https://nixos.org/nix/) package manager to provide a
build environment suitable for running shake. Shakebook has heavy dependencies
due to latex, pandoc, and image renderers, so you will want to install
[cachix](cachix.org) and use the cache at shakebook.cachix.org. Run

```{.bash}
cachix use shakebook
```

to set this up.

## Building

First clone the template repository
[here](https://gitlab.com/shakebook-site/shakebook-site.gitlab.io).

Check that you can drop into a nix shell by running the command

```{.bash}
nix-shell
```

Nix should pull in all the dependencies and set up the build environment for
you.

One you're in the shell, you can just run

```
shake
```

and shake will grab all the content in the `site/` directory, compile it, and
dump it in the `public/` directory,

## Serving

You can use warp, which is provided in the nix-shell, by running

```
warp -d public
```

Then navigate to your site at localhost:3000
