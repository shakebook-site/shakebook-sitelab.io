---
title: Updating The Build Plan
description: Update stackage manifest and haskell.nix
modified: 2020-08-06T10:31:00Z
---

# Updating The Build Plan

If you want to update the build plan for yourself. You can use these commands.

To update the nixpkgs base:

```
nix-prefetch-git https://github.com/NixOS/nixpkgs | tee nix/nixpkgs-src.json
```

To update the haskell.nix package set

```
nix-prefetch-git https://github.com/input-output-hk/haskell.nix | tee nix/haskell-nix-src.json
```

If you make modifications to the `package.yaml` or to the `stack.yaml`, you
will need to run `stack-to-nix` using the [haskell.nix
tools](https://input-output-hk.github.io/haskell.nix/user-guide/stack-projects/).

```
stack-to-nix -o nix/ --stack-yaml stack.yaml
```

Remember, any modifications you make to the build plan will bring you out of
lockstep with Shakebook's official cache, at which point you may want to set up
[your own](https://cachix.org/).

