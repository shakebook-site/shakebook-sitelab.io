---
title: Markdown and Table Of Contents
description: Adding markdown documents to your shakebook.
modified: 2020-08-06T10:31:00Z
---

# Markdown & Table of Contents

Adding new markdown documents to your Shakebook is as simple as adding a new
document in the `site/docs` folder and then updating the table of contents
in the `Shakefile.hs`.

The table of contents itself is represented as a
[Rosetree](https://en.wikipedia.org/wiki/Rose_tree), (or alternatively a
`Cofree [] (Path Rel File)`). That is, a section header document followed by a list of
subsections, recursively. We stop at 3 layers by convention, but some second
level sections have no subsections.

```{.haskell}
tableOfContents :: Cofree [] (Path Rel File)
tableOfContents = $(mkRelFile "docs/index.md") :< [
                    $(mkRelFile "docs/1/index.md") :< []
                  , $(mkRelFile "docs/2/index.md") :< [
                  $(mkRelFile "docs/2/champ.md") :< []
                  ]
                ]
```

Note, at present you must perform an `rm -rf public .shake` before rerunning
`shake` if you change the Shakefile itself. The Shakefile does not track itself
as a build dependency.

A markdown document is a yaml header followed by a bunch of markdown.

```{.markdown}
---
title: "My Document"
author: Me
tags: [shakebook]
description: A description of my document
---

# My Header

Some text

* A bullet point
```

Note, only the HTML table of contents will use the yaml `title` field to
display the name, whereas the table of contents in the PDF export will use the
hash headers in the markdown body itself.
