{-# LANGUAGE BlockArguments    #-}
{-# LANGUAGE ConstraintKinds   #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE TypeOperators     #-}

module Shakefile where

import           Composite.Aeson
import           Composite.Record
import           Composite.Record.Binary         ()
import           Composite.Record.Hashable       ()
import qualified Composite.Record.Tuple          as C
import           Composite.XStep
import qualified Data.IxSet.Typed                as Ix
import qualified Data.IxSet.Typed.Conversions    as Ix
import           Data.Vinyl                      hiding (RElem)
import           Development.Shake.Plus.Extended
import           Development.Shake.Plus.Forward
import           Lucid hiding (command_)
import           Lucid.CDN
import           Path.Extensions
import           Path.Utils
import           RIO
import qualified RIO.ByteString.Lazy             as LBS
import qualified RIO.Text                        as T
import           RIO.Directory
import           Shakebook                       hiding ((:->))
import           Text.Compdoc

---Config-----------------------------------------------------------------------

siteTitle :: Text
siteTitle = "Shakebook"

-- The BaseURL of your site, mainly used for Atom Feeds
baseUrl :: Text
baseUrl = "https://shakebook.site"

-- The title of your Atom Feed.
feedTitle :: Text
feedTitle = "Shakebook Feed"

-- The output folder 'public', which is picked up by gitlab-ci.
outputDir :: Path Rel Dir
outputDir = $(mkRelDir "public")

-- The source folder 'site', containing your site source.
sourceDir :: Path Rel Dir
sourceDir = $(mkRelDir "site")

postsDir :: Path Rel Dir
postsDir = $(mkRelDir "posts")

tagsDir :: Path Rel Dir
tagsDir = $(mkRelDir "tags")

monthsDir :: Path Rel Dir
monthsDir = $(mkRelDir "months")

templatesDir :: Path Rel Dir
templatesDir = $(mkRelDir "templates")

-- Posts per page
postsPerPage :: Int
postsPerPage = 5

-- Amount of recent posts to display
numRecentPosts :: Int
numRecentPosts = 5

-- Numer of page numbers to display around this one.
numPageNeighbours :: Int
numPageNeighbours = 2

mySocial :: [Record Link]
mySocial = []

-- The table of contents, this is a RoseTree. A head element a followed by a list of
-- subsections.
tableOfContents :: Cofree [] (Path Rel File)
tableOfContents =
  $(mkRelFile "docs/index.md") :< [
    $(mkRelFile "docs/getting-started/index.md") :< []
  , $(mkRelFile "docs/content-and-customization/index.md") :< [
      $(mkRelFile "docs/content-and-customization/markdown-table-of-contents.md") :< []
    , $(mkRelFile "docs/content-and-customization/image-compilation-units.md") :< []
    ]
  , $(mkRelFile "docs/build-management/index.md") :< [
      $(mkRelFile "docs/build-management/updating-build-plan.md") :< []
    ]
  ]

-- The animation format for reanimate videos
animationFormat :: String
animationFormat = ".mp4"

-- Animation options for reanimate. Default setting is to use imagemagick on low settings.
animationOptions :: [String]
animationOptions = ["--format", drop 1 animationFormat, "--raster", "imagemagick", "--preset", "quick"]

cdnImports :: Html ()
cdnImports = do
  bootstrapCSS_4_5_2
  bootstrapJS_4_5_2
  fontawesome_4_7_0
  mathjax_3_1_0
  jquery_3_5_1
  popper_2_4_4

--- Custom Build Rules ---

{--
buildPDF :: (MonadThrow m, MonadAction m) => Cofree [] (Path Rel File) -> Path Rel File -> Path Rel File -> m ()
buildPDF toc meta out = do
  k <- mapM (loadMarkdownWith defaultMarkdownReaderOptions sourceDir) toc
  a <- readMDFileIn defaultMarkdownReaderOptions sourceDir meta
  z <- replaceUnusableImages [".mp4"] (defaultVideoReplacement baseUrl) $ foldr (<>) a $ progressivelyDemoteHeaders k
  needPandocImagesIn outputDir z
  let z' = prefixAllImages outputDir z
  makePDFLaTeX defaultLatexWriterOptions z' >>= LBS.writeFile (toFilePath out)
--}
--- Custom Fields ---

stage1PostExtras :: (MonadAction m, MonadThrow m) => Path Rel File -> XStep' m RawPost Stage1PostExtras
stage1PostExtras x = pure . view fPosted
                 ::& pure . map (deriveTagLink tagRoot . Tag) . view fTags
                 ::& pure . defaultDeriveTeaser . view fContent
                 ::& deriveUrl . const x
                 ::& XRNil

stage1DocExtras :: MonadThrow m => Path Rel File -> XStep' m RawDoc Stage1DocExtras
stage1DocExtras x = deriveUrl . const x ::& XRNil

createBlogNav :: PostSet -> Cofree [] (Record Link)
createBlogNav xs = ("Blog" :*: "/posts/" :*: RNil) :<
                   Ix.toDescCofreeList (C.fanout (defaultPrettyMonthFormat . fromYearMonth) monthRoot)
                                       (C.fanout (view fTitle) (view fUrl))
                                       (Down . view fPosted)
                                       xs

type FNavCover = "nav-class" :-> Text

type MyMainPage = FNavCover : MainPage

myMainPageJsonFields :: Rec (JsonField e) MyMainPage
myMainPageJsonFields = rcast $ (field textJsonFormat :: JsonField e FNavCover) :& mainPageJsonFields

createDocNav :: Cofree [] (Record Stage1Doc) -> Cofree [] (Record Link)
createDocNav = fmap (C.fanout (view fTitle) (view fUrl))

loadMarkdownWith :: (ReifyConstraint Show Identity (Compdoc a), RMap (Compdoc a), Binary (Record (Compdoc a)), Typeable (Record (Compdoc a)), RecordToList (Compdoc a), MonadSB r m) => JsonFormat Void (Record a) -> Path Rel File -> m (Record (Compdoc a))
loadMarkdownWith f x = cacheAction ("loader" :: Text, x) $ do
  logInfo $ "Loading " <> displayShow (toFilePath x)
  readMarkdownFile defaultMarkdownReaderOptions defaultHtml5WriterOptions f x

loadRawPost :: MonadSB r m => Path Rel File -> m (Record RawPost)
loadRawPost = loadMarkdownWith rawPostMetaJsonFormat

loadRawSingle :: MonadSB r m => Path Rel File -> m (Record RawSingle)
loadRawSingle = loadMarkdownWith rawSingleMetaJsonFormat

loadRawDoc :: MonadSB r m => Path Rel File -> m (Record RawDoc)
loadRawDoc = loadMarkdownWith rawDocMetaJsonFormat

deriveUrl :: MonadThrow m => Path Rel File -> m Text
deriveUrl = fmap toGroundedUrl . withHtmlExtension <=< stripProperPrefix sourceDir

enrichment :: Record Enrichment
enrichment = mySocial :*: toHtmlFragment cdnImports :*: toStyleFragment defaultHighlighting :*: siteTitle :*: RNil

myBuildPage :: (MonadAction m, RMap x, RecordToJsonObject x, RecordFromJson x)
            => PName -> Rec (JsonField e) x -> Record x -> Path Rel File -> m ()
myBuildPage t f x out = do
  k <- compileMustacheDir' t (sourceDir </> templatesDir)
  let l' = renderMustache' k (enrichedRecordJsonFormat f) (enrichment <+> x)
  writeFile' out l'

buildIndex :: MonadSB r m => Record MyMainPage -> Path Rel File -> m ()
buildIndex = myBuildPage "index" myMainPageJsonFields

buildPost :: MonadSB r m => Record FinalPost -> Path Rel File -> m ()
buildPost = myBuildPage "post" finalPostJsonFields

buildDoc :: MonadSB r m => Record FinalDoc -> Path Rel File -> m ()
buildDoc = myBuildPage "docs" finalDocJsonFields

buildPostIndex :: MonadSB r m => Record (IndexPage Stage1Post) -> Path Rel File -> m ()
buildPostIndex = myBuildPage "post-list" postIndexPageJsonFields

docsRules :: MonadSB r m => Path Rel Dir -> Cofree [] (Path Rel File) -> m ()
docsRules dir toc = do
  as <- mapM (\x -> loadMarkdownWith rawDocMetaJsonFormat x >>= prependXStep (stage1DocExtras x)) (fmap (sourceDir </>) toc)
  let nav = createDocNav as
  sequence_ $ as =>> \(x :< xs) -> do
    out <- fromGroundedUrlF (view fUrl x)
    let v = nav :*: (extract <$> xs) :*: x
    buildDoc v (outputDir </> out)

postIndex :: MonadSB r m => Path Rel Dir -> [FilePattern] -> m PostSet
postIndex = batchLoadIndex (\x -> loadMarkdownWith rawPostMetaJsonFormat x >>= prependXStep (stage1PostExtras x))

postsRoot :: Text
postsRoot  = toGroundedUrl postsDir

tagRoot :: Tag -> Text
tagRoot = (\x -> toGroundedUrl (postsDir </> tagsDir) <> x <> "/") . unTag

monthRoot :: YearMonth -> Text
monthRoot = (\x -> toGroundedUrl (postsDir </> monthsDir) <> x <> "/") . defaultMonthUrlFormat . fromYearMonth

mainPageExtras :: PostSet -> Record (FNavCover : FRecentPosts Stage1Post : '[])
mainPageExtras xs = "td-navbar-cover" :*: recentPosts numRecentPosts xs :*: RNil

mainPageRules :: MonadSB r m => m ()
mainPageRules = do
  postIx <- postIndex sourceDir ["posts/*.md"]
  x <- loadRawSingle $ sourceDir </> $(mkRelFile "index.md")
  let x' = mainPageExtras postIx <+> x
  buildIndex x' $ outputDir </> $(mkRelFile "index.html")

indexPages :: MonadThrow m => PostSet -> Text -> m (Zipper [] (Record (FUrl : FItems Stage1Post : FPageNo : '[])))
indexPages postIx f = do
  p <- paginate' postsPerPage $ Ix.toDescList (Proxy @Posted) postIx
  return $ p =>> \a -> f <> "pages/" <> T.pack (show $ pos a + 1) :*: extract a :*: pos a + 1 :*: RNil

postIndexRules :: MonadSB r m => Cofree [] (Record Link) -> Text -> PostSet -> Text -> m ()
postIndexRules nav title postset root = do
    ys <- indexPages postset root
    sequence_ $ ys =>> \xs' -> do
      let x = extract xs'
      out <- (</> $(mkRelFile "index.html")) <$> fromGroundedUrlD (view fUrl x)
      ps <- toHtmlFragmentM $ renderPageLinks numPageNeighbours ys
      let x' = ps :*: nav :*: title :*: x
      buildPostIndex x' (outputDir </> out)
    let k = extract $ seek 0 ys
    k' <- (</> $(mkRelFile "index.html")) <$> fromGroundedUrlD (view fUrl k)
    s' <- (</> $(mkRelFile "index.html")) <$> fromGroundedUrlD root
    copyFileChanged (outputDir </> k') (outputDir </> s')

postRules :: MonadSB r m => Path Rel Dir -> [FilePattern] -> m PostSet
postRules dir fp = cacheAction ("build" :: T.Text, (dir, fp)) $ do
  postsIx <- postIndex dir fp
  let nav = createBlogNav postsIx
  let postsZ = Ix.toDescList (Proxy @Posted) postsIx
  forM_ postsZ $ \x -> do
    out <- fromGroundedUrlF (view fUrl x)
    let x' = nav :*: x
    buildPost x' (outputDir </> out)
  postIndexRules nav "Posts" postsIx postsRoot
  forM_ (Ix.indexKeys postsIx) $ \t@(Tag t') ->
    postIndexRules nav ("Posts tagged " <> t') (postsIx Ix.@+ [t]) (tagRoot t)
  forM_ (Ix.indexKeys postsIx) \ym@(YearMonth _) ->
    postIndexRules nav ("Posts from " <> defaultPrettyMonthFormat (fromYearMonth ym)) (postsIx Ix.@+ [ym]) (monthRoot ym)
  return postsIx

sitemapRules :: MonadSB r m => PostSet -> Path Rel File -> m ()
sitemapRules xs out = cacheAction ("sitemap" :: T.Text, out) $
  LBS.writeFile (toFilePath $ outputDir </> out) $ renderSitemap $ Sitemap $ fmap (asSitemapUrl baseUrl) $ Ix.toList xs

copyStaticFiles :: MonadSB r m => m ()
copyStaticFiles = do
  filepaths <- getDirectoryFiles sourceDir ["images//*", "css//*", "js//*"]
  void $ forP filepaths $ \filepath -> copyFileChanged (sourceDir </> filepath) (outputDir </> filepath)

runHaskell :: MonadSB r m => [String] -> Path Rel File -> m ()
runHaskell opts src = do
  cacheAction ("runhaskell" :: T.Text, src) $ command_ [] "runhaskell" $ [toFilePath src] ++ opts

buildAnimation :: MonadSB r m => Path Rel File -> m ()
buildAnimation src = do
  out <- (outputDir </>) <$> withMp4Extension src
  createDirectoryIfMissing True (toFilePath $ parent out)
  runHaskell (["render", "-o", toFilePath out] ++ animationOptions) (sourceDir </> src)

buildRPlot :: MonadSB r m => Path Rel File -> m ()
buildRPlot src = do
  out <- (outputDir </>) <$> withPngExtension src
  createDirectoryIfMissing True (toFilePath $ parent out)
  runHaskell ["-o", toFilePath out] (sourceDir </> src)

animationRules :: MonadSB r m => m ()
animationRules = do
  xs <- getDirectoryFiles sourceDir ["animations/*.hs"]
  forM_ xs $ buildAnimation

rPlotRules :: MonadSB r m => m ()
rPlotRules = do
  xs <- getDirectoryFiles sourceDir ["plots/*.hs"]
  forM_ xs $ buildRPlot

buildRules = do
  mainPageRules
  xs <- postRules sourceDir ["posts/*.md"]
  docsRules sourceDir tableOfContents
  sitemapRules xs $(mkRelFile "sitemap.xml")
  copyStaticFiles
  animationRules
--  rPlotRules

main :: IO ()
main = do
  lo <- logOptionsHandle stderr True
  (lf, dlf) <- newLogFunc (setLogMinLevel LevelInfo lo)
  let shOpts = shakeOptions { shakeLintInside = ["\\"]}
  shakeArgsForward shOpts lf buildRules
  dlf

