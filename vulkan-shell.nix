{ sources ? import ./nix/sources.nix {}
, pkgs ? import sources.nixpkgs {} }:
let
  haskellPackages = with pkgs.haskell.lib;
  pkgs.haskellPackages.override {
    overrides = self: super: {
      vulkan-utils = addExtraLibrary
          (doHaddock (self.callCabal2nix "" (sources.vulkan + "/utils") { }))
          pkgs.vulkan-headers;
      VulkanMemoryAllocator = addExtraLibrary (doHaddock
        (self.callCabal2nix "" (sources.vulkan + "/VulkanMemoryAllocator")
          { })) pkgs.vulkan-headers;
      vulkan = doHaddock (self.callCabal2nix "vulkan" sources.vulkan {
        vulkan = pkgs.vulkan-loader;
      });
      th-desugar = self.callCabal2nix "" (pkgs.fetchFromGitHub {
        owner = "goldfirere";
        repo = "th-desugar";
        rev = "f075206882ce4e554c37537e624b4be7409d74a3";
        sha256 = "0747xggx2q8yphag2wv06dj0pgi9zvadi069c2d6lckg26chhnlk";
      }) { };
      th-orphans = self.callCabal2nix "" (pkgs.fetchFromGitHub {
        owner = "mgsloan";
        repo = "th-orphans";
        rev = "713b4e14890b856b52a5f9f57e0cd06d99c5195b";
        sha256 = "0482j056nxf7bns4qzkkv40gi61f2z5jnyg1d5ks1l01gjfdrqkp";
      }) { };
      autoapply = doHaddock (self.callCabal2nix "" (pkgs.fetchFromGitHub {
        owner = "expipiplus1";
        repo = "autoapply";
        rev = "4ff481b28c9f2b081496593bba491633873ca155";
        sha256 = "0hfcx9mnan0f5h5x8qvpzybbvn6brmia7s2wfgk8j61arghfwg8k";
      }) { });
    };
  };
in
  haskellPackages.ghcWithPackages(ps: with ps; [vulkan-utils vulkan VulkanMemoryAllocator autoapply])
