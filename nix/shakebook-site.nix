let
  buildDepError = pkg:
    builtins.throw ''
      The Haskell package set does not contain the package: ${pkg} (build dependency).
      
      If you are using Stackage, make sure that you are using a snapshot that contains the package. Otherwise you may need to update the Hackage snapshot you are using, usually by updating haskell.nix.
      '';
  sysDepError = pkg:
    builtins.throw ''
      The Nixpkgs package set does not contain the package: ${pkg} (system dependency).
      
      You may need to augment the system package mapping in haskell.nix so that it can be found.
      '';
  pkgConfDepError = pkg:
    builtins.throw ''
      The pkg-conf packages does not contain the package: ${pkg} (pkg-conf dependency).
      
      You may need to augment the pkg-conf package mapping in haskell.nix so that it can be found.
      '';
  exeDepError = pkg:
    builtins.throw ''
      The local executable components do not include the component: ${pkg} (executable dependency).
      '';
  legacyExeDepError = pkg:
    builtins.throw ''
      The Haskell package set does not contain the package: ${pkg} (executable dependency).
      
      If you are using Stackage, make sure that you are using a snapshot that contains the package. Otherwise you may need to update the Hackage snapshot you are using, usually by updating haskell.nix.
      '';
  buildToolDepError = pkg:
    builtins.throw ''
      Neither the Haskell package set or the Nixpkgs package set contain the package: ${pkg} (build tool dependency).
      
      If this is a system dependency:
      You may need to augment the system package mapping in haskell.nix so that it can be found.
      
      If this is a Haskell dependency:
      If you are using Stackage, make sure that you are using a snapshot that contains the package. Otherwise you may need to update the Hackage snapshot you are using, usually by updating haskell.nix.
      '';
in { system, compiler, flags, pkgs, hsPkgs, pkgconfPkgs, ... }:
  ({
    flags = {};
    package = {
      specVersion = "0";
      identifier = { name = "shakebook-site"; version = "0.1.0.0"; };
      license = "BSD-3-Clause";
      copyright = "20XX Me";
      maintainer = "example@example.com";
      author = "Me";
      homepage = "";
      url = "";
      synopsis = "Shake-based markdown/latex webbook.";
      description = "";
      buildType = "Simple";
      isLocal = true;
      };
    components = {
      exes = {
        "build-site" = {
          depends = [
            (hsPkgs."aeson" or (buildDepError "aeson"))
            (hsPkgs."base" or (buildDepError "base"))
            (hsPkgs."comonad-extras" or (buildDepError "comonad-extras"))
            (hsPkgs."compdoc" or (buildDepError "compdoc"))
            (hsPkgs."composite-aeson" or (buildDepError "composite-aeson"))
            (hsPkgs."composite-base" or (buildDepError "composite-base"))
            (hsPkgs."composite-binary" or (buildDepError "composite-binary"))
            (hsPkgs."composite-hashable" or (buildDepError "composite-hashable"))
            (hsPkgs."composite-tuple" or (buildDepError "composite-tuple"))
            (hsPkgs."composite-xstep" or (buildDepError "composite-xstep"))
            (hsPkgs."feed" or (buildDepError "feed"))
            (hsPkgs."free" or (buildDepError "free"))
            (hsPkgs."inline-r" or (buildDepError "inline-r"))
            (hsPkgs."ixset-typed" or (buildDepError "ixset-typed"))
            (hsPkgs."ixset-typed-conversions" or (buildDepError "ixset-typed-conversions"))
            (hsPkgs."lens" or (buildDepError "lens"))
            (hsPkgs."lens-aeson" or (buildDepError "lens-aeson"))
            (hsPkgs."lucid" or (buildDepError "lucid"))
            (hsPkgs."pandoc" or (buildDepError "pandoc"))
            (hsPkgs."pandoc-types" or (buildDepError "pandoc-types"))
            (hsPkgs."path-extensions" or (buildDepError "path-extensions"))
            (hsPkgs."path-utils" or (buildDepError "path-utils"))
            (hsPkgs."reanimate" or (buildDepError "reanimate"))
            (hsPkgs."rio" or (buildDepError "rio"))
            (hsPkgs."shake-plus" or (buildDepError "shake-plus"))
            (hsPkgs."shake-plus-extended" or (buildDepError "shake-plus-extended"))
            (hsPkgs."shakebook" or (buildDepError "shakebook"))
            (hsPkgs."split" or (buildDepError "split"))
            (hsPkgs."vinyl" or (buildDepError "vinyl"))
            ];
          buildable = true;
          };
        };
      };
    } // rec { src = (pkgs.lib).mkDefault .././.; }) // {
    cabal-generator = "hpack";
    }