{
  extras = hackage:
    {
      packages = {
        "compdoc" = (((hackage.compdoc)."0.1.0.0").revisions).default;
        "composite-aeson-throw" = (((hackage.composite-aeson-throw)."0.1.0.0").revisions).default;
        "composite-tuple" = (((hackage.composite-tuple)."0.1.0.0").revisions).default;
        "composite-xstep" = (((hackage.composite-xstep)."0.1.0.0").revisions).default;
        "commonmark" = (((hackage.commonmark)."0.1.0.2").revisions).default;
        "commonmark-extensions" = (((hackage.commonmark-extensions)."0.2.0.1").revisions).default;
        "commonmark-pandoc" = (((hackage.commonmark-pandoc)."0.2.0.0").revisions).default;
        "lucid-cdn" = (((hackage.lucid-cdn)."0.2.0.1").revisions).default;
        "pandoc" = (((hackage.pandoc)."2.10.1").revisions).default;
        "pandoc-throw" = (((hackage.pandoc-throw)."0.1.0.0").revisions).default;
        inline-r = ./inline-r.nix;
        path-utils = ./path-utils.nix;
        shakebook = ./shakebook.nix;
        };
      };
  resolver = "nightly-2020-09-06";
  modules = [ ({ lib, ... }: { packages = {}; }) { packages = {}; } ];
  }