{ sources ? import ./sources.nix
, haskellNix ? import sources.haskell {}
, pkgs ? import haskellNix.sources.nixpkgs-2003 haskellNix.nixpkgsArgs
}: pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "shakebook-template";
    src = ../.;
  };
}
